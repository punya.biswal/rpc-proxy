REGISTRY = "registry.gitlab.com/punya.biswal/rpc-proxy"

def bazel_query(q):
    raw = local("bazel query '%s' --order_output=no" % q)
    return str(raw).splitlines()

def external_label(label):
    return (
        label.startswith("@") or
        label.startswith("//external/") or
        label.startswith("//external:")
    )

def label_to_path(label):
    if label.startswith("//"):
        label = label[2:]
    path = label.replace(":", "/")
    if path.startswith("/"):
        path = path[1:]
    return path

def watch_labels(target):
    build_deps = bazel_query('filter("^//", kind("source file", deps(set(%s))))' % target)
    source_deps = bazel_query('filter("^//", buildfiles(deps(set(%s))))' % target)
    return [
        label_to_path(label)
        for label in (build_deps + source_deps)
        if not external_label(label)
    ]

def bazel_k8s(target):
    for file in watch_labels(target):
        watch_file(file)
    return local("bazel run %s" % target)

def bazel_build(image, target):
    custom_build(
        image + ":image",
        "bazel run %s -- --norun" % target,
        watch_labels(target),
        tag="image",
        disable_push=True,
    )

default_registry(REGISTRY)

for name in ["rpc-adapter", "rpc-server"]:
    bazel_build(
        "%s/%s" % (REGISTRY, name),
        "//%s:image" % name,
    )

k8s_yaml(bazel_k8s("//:k8s"))

k8s_resource("rabbitmq", port_forwards=15672)
