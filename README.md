rpc-proxy
=========

A project that demonstrates a lot of developer tooling, in addition to getting
something useful done.

```mermaid
sequenceDiagram

  participant rabbit_mq
  participant rpc_adapter
  participant rpc_server

  rpc_adapter ->> rabbit_mq: subscribe to rpc_queue;
  rabbit_mq ->>+ rpc_adapter: GreetRequest;
  rpc_adapter ->>+ rpc_server: Example.Greet();
  rpc_server -->>- rpc_adapter: GreetResponse;
  rpc_adapter -->>- rabbit_mq: publish GreetResponse;
```
