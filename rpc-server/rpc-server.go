package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"time"

	"gitlab.com/punya.biswal/rpc-proxy/pb"
	"google.golang.org/grpc"
)

type s struct{}

func (s) Greet(ctx context.Context, req *pb.GreetRequest) (*pb.GreetResponse, error) {
	ts := req.GetTimestamp()
	t := time.Unix(ts.GetSeconds(), int64(ts.GetNanos()))
	return &pb.GreetResponse{
		Greeting: fmt.Sprintf("Hello %s at %s", req.GetName(), t.Format("15:04")),
	}, nil
}

func main() {
	log.Print("Starting rpc-server")
	lis, err := net.Listen("tcp", fmt.Sprintf("localhost:8080"))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	server := grpc.NewServer()
	pb.RegisterExampleServer(server, s{})

	log.Print("Listening for RPC requests")
	server.Serve(lis)
}
