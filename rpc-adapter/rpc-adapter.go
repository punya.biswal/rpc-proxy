package main

import (
	"context"
	"log"

	"github.com/golang/protobuf/proto"
	"github.com/streadway/amqp"
	"gitlab.com/punya.biswal/rpc-proxy/pb"
	"google.golang.org/grpc"
)

func main() {
	log.Print("Starting rpc-adapter")
	onClose := make(chan *amqp.Error)

	conn, err := amqp.Dial("amqp://guest:guest@rabbitmq:5672/")
	if err != nil {
		panic(err)
	}
	defer conn.Close()
	conn.NotifyClose(onClose)

	ch, err := conn.Channel()
	if err != nil {
		panic(err)
	}
	defer ch.Close()

	q, err := ch.QueueDeclare("rpc_queue", false, false, false, false, nil)
	if err != nil {
		panic(err)
	}

	err = ch.Qos(1, 0, false)
	if err != nil {
		panic(err)
	}

	msgs, err := ch.Consume(q.Name, "", false, false, false, false, nil)
	if err != nil {
		panic(err)
	}

	grpcConn, err := grpc.Dial("localhost:8080", grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	defer grpcConn.Close()

	exampleClient := pb.NewExampleClient(grpcConn)
	exampleClient.Greet(context.Background(), nil)

	log.Print("Entering message loop")
	for {
		select {
		case d := <-msgs:
			var req pb.GreetRequest
			err = proto.Unmarshal(d.Body, &req)
			if err != nil {
				log.Print(err)
				continue
			}

			resp, err := exampleClient.Greet(context.Background(), &req)
			if err != nil {
				log.Print(err)
				continue
			}

			var body []byte
			body, err = proto.Marshal(resp)
			if err != nil {
				log.Print(err)
				continue
			}

			err = ch.Publish("", d.ReplyTo, false, false, amqp.Publishing{
				ContentType:   "application/protobuf",
				CorrelationId: d.CorrelationId,
				Body:          body,
			})
			if err != nil {
				log.Print(err)
				continue
			}

		case <-onClose:
			return
		}
	}
}
