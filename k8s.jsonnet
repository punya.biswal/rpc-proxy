local registry = 'registry.gitlab.com/punya.biswal/rpc-proxy';

local app = {
  metadata:: {
    name: '',
    labels: { app: $.metadata.name },
  },

  deployment:: {
    apiVersion: 'apps/v1',
    kind: 'Deployment',
    metadata: $.metadata,
    spec: {
      selector: { matchLabels: $.metadata.labels },
      template: {
        metadata: $.metadata,
        spec: { containers: [] },
      },
    },
  },

  service:: {
    apiVersion: 'v1',
    kind: 'Service',
    metadata: $.metadata,
    spec: { selector: $.metadata.labels },
  },
};

local rabbitmq = app {
  metadata+:: { name: 'rabbitmq' },
  deployment+:: { spec+: { template+: { spec+: { containers+: [
    {
      name: 'rabbitmq',
      image: 'rabbitmq:management',
      ports: [
        { containerPort: 5672 },
        { containerPort: 15672 },
      ],
      resources: { cpu: '200m' },
    },
  ] } } } },
  service+:: { spec+: { ports+: [
    { name: 'amqp', port: 5672 },
    { name: 'management', port: 15672 },
  ] } },
};

local greeter = app {
  metadata+:: { name: 'greeter' },
  deployment+:: {
    spec+: { template+: { spec+: { containers+: [
      {
        name: 'rpc-server',
        image: registry + '/rpc-server',
        ports: [{ containerPort: 8080 }],
        resources: { cpu: '10m' },
      },
      {
        name: 'rpc-adapter',
        image: registry + '/rpc-adapter',
        resources: { cpu: '10m' },
      },
    ] } } },
  },
};

[
  rabbitmq.deployment,
  rabbitmq.service,
  greeter.deployment,
]
