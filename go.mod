module gitlab.com/punya.biswal/rpc-proxy

go 1.12

require (
	github.com/golang/protobuf v1.3.1
	github.com/streadway/amqp v0.0.0-20190404075320-75d898a42a94
	google.golang.org/grpc v1.20.1
)
